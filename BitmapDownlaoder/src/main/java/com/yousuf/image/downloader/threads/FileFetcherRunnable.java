package com.yousuf.image.downloader.threads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.yousuf.image.downloader.model.Extras;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Runnable that fetches the bitmap from internal storage or from network.
 */
public class FileFetcherRunnable implements Runnable {

    public interface TaskRunnableFileFetcherMethods {
        File getFile();

        void setByteBuffer(byte[] buffer);

        void setFileFetcherThread(Runnable thread);

        void setCurrentThread(Thread currentThread);

        void handleDownloadState(int state);
    }

    private TaskRunnableFileFetcherMethods mBitmapLoader;

    public FileFetcherRunnable(TaskRunnableFileFetcherMethods bitmapLoader) {
        mBitmapLoader = bitmapLoader;
    }


    @Override
    public void run() {
        mBitmapLoader.setCurrentThread(Thread.currentThread());

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        byte[] byteBuffer = null;

        try {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            File file = mBitmapLoader.getFile();

            if (file != null && file.exists()) {
                byteBuffer = decodeFile(file);
            }

            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

        } catch (IOException | InterruptedException e1) {
            if(e1.getMessage() != null) {
                Log.d("FileLoader", e1.getMessage());
            }
            e1.printStackTrace();
        } finally {
            // If the byteBuffer is null, reports that the fetch failed.
            if (null == byteBuffer) {
                mBitmapLoader.handleDownloadState(Extras.FILE_FETCH_FAILED);
            } else {
                mBitmapLoader.setByteBuffer(byteBuffer);
                mBitmapLoader.handleDownloadState(Extras.FILE_FOUND);
            }
            mBitmapLoader.setFileFetcherThread(null);
            Thread.interrupted();
        }
    }


    /**
     * Decode file to bitmap
     *
     * @param file file
     * @return Decoded Bitmap
     */
    public byte[] decodeFile(File file) throws IOException {
        DataInputStream dataIS = new DataInputStream(new FileInputStream(file));
        byte[] buffer = new byte[(int)file.length()];
        dataIS.readFully(buffer);
        dataIS.close();
        return buffer;
    }
}