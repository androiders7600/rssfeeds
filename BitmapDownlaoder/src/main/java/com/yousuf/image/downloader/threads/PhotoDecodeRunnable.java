/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yousuf.image.downloader.threads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.yousuf.image.downloader.model.Extras;

/**
 * This runnable decodes a byte array containing an image.
 * <p/>
 * Objects of this class are instantiated and managed by instances of PhotoTask, which
 * implements the methods {@link TaskRunnableDecodeMethods}. PhotoTask objects call
 * {@link #PhotoDecodeRunnable(TaskRunnableDecodeMethods) PhotoDecodeRunnable()} with
 * themselves as the argument. In effect, an PhotoTask object and a
 * PhotoDecodeRunnable object communicate through the fields of the PhotoTask.
 */
public class PhotoDecodeRunnable implements Runnable {

    private static final int NUMBER_OF_DECODE_TRIES = 2;

    /**
     * An interface that defines methods that PhotoTask implements. An instance of
     * PhotoTask passes itself to an PhotoDecodeRunnable instance through the
     * PhotoDecodeRunnable constructor, after which the two instances can access each other's
     * variables.
     */
    public interface TaskRunnableDecodeMethods {

        void setImageDecodeThread(Runnable currentThread);

        byte[] getByteBuffer();

        void handleDecodeState(int state);

        int getTargetWidth();

        int getTargetHeight();

        void setImage(Bitmap image);

    }

    // Defines a field that contains the calling object of type PhotoTask.
    final TaskRunnableDecodeMethods mPhotoTask;

    public PhotoDecodeRunnable(TaskRunnableDecodeMethods downloadTask) {
        mPhotoTask = downloadTask;
    }

    @Override
    public void run() {

        /*
         * Stores the current Thread in the the PhotoTask instance, so that the instance
         * can interrupt the Thread.
         */
        mPhotoTask.setImageDecodeThread(Thread.currentThread());
        byte[] imageBuffer = mPhotoTask.getByteBuffer();
        Bitmap returnBitmap = null;

        try {
            mPhotoTask.handleDecodeState(Extras.DECODE_STARTED);

            // Before continuing, checks to see that the Thread hasn't
            // been interrupted
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            for (int i = 0; i < NUMBER_OF_DECODE_TRIES; i++) {
                try {
                    /*
                     * Sets the desired image height and width based on the
                     * ImageView being created.
                     */
                    int targetWidth = mPhotoTask.getTargetWidth();
                    int targetHeight = mPhotoTask.getTargetHeight();
                    int sampleSize = getScale(imageBuffer, targetWidth, targetHeight);
                    if (Thread.interrupted()) {
                        return;
                    }
                    returnBitmap = decodeScaledBitmap(imageBuffer, sampleSize);

                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }

                } catch (Throwable e) {
                    if(e.getMessage() != null) {
                        Log.d("PhotoDecoder", e.getMessage());
                    }

                    java.lang.System.gc();

                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }

                    try {
                        Thread.sleep(Extras.SLEEP_TIME_MILLISECONDS);
                    } catch (java.lang.InterruptedException interruptException) {
                        return;
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (null == returnBitmap) {
                mPhotoTask.handleDecodeState(Extras.FAILED);
            } else {
                mPhotoTask.setImage(returnBitmap);
                mPhotoTask.handleDecodeState(Extras.TASK_COMPLETE);
            }

            mPhotoTask.setImageDecodeThread(null);
            Thread.interrupted();
        }

    }

    /**
     *
     * @param imageBuffer
     * @param targetWidth
     * @param targetHeight
     * @return
     */
    private int getScale(byte[] imageBuffer, int targetWidth, int targetHeight) {
        // Sets up options for creating a Bitmap object from the
        // downloaded image.
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageBuffer, 0, imageBuffer.length, bitmapOptions);
        int hScale = bitmapOptions.outHeight / targetHeight;
        int wScale = bitmapOptions.outWidth / targetWidth;
        return Math.max(hScale, wScale);
    }

    /**
     *
     * @param imageBuffer
     * @param sampleSize
     * @return
     */
    private Bitmap decodeScaledBitmap(byte[] imageBuffer, int sampleSize) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        if (sampleSize > 1) {
            bitmapOptions.inSampleSize = sampleSize;
        }
        bitmapOptions.inJustDecodeBounds = false;
        // Tries to decode the image buffer
        return BitmapFactory.decodeByteArray(imageBuffer, 0, imageBuffer.length, bitmapOptions);

    }
}
