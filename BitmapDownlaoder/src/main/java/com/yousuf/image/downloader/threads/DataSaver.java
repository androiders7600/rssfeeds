package com.yousuf.image.downloader.threads;

import com.yousuf.image.downloader.ImageDownloadManager;
import com.yousuf.image.downloader.cache.FileCache;

import java.io.File;

/**
 * Created by u471637 on 4/16/16.
 */
public class DataSaver implements BitmapSaverRunnable.TaskRunnableBitmapSaverMethods{

    private Runnable mFileSaverRunnable;

    private byte[] mBitmapBytes;

    private Thread mCurrentThread;

    private String mUrl;

    public void initDataSaver(byte[] buffer,String url){
        mFileSaverRunnable = new BitmapSaverRunnable(this);
        mBitmapBytes = buffer.clone();
        mUrl = new String(url);
    }

    @Override
    public void handleSaveState(int state) {
        ImageDownloadManager.getInstance().handleSaveRequestStates(this, state);
    }

    @Override
    public byte[] getImageBuffer() {
        return mBitmapBytes;
    }

    @Override
    public File getFile() {
        FileCache mFileCache = ImageDownloadManager.getInstance().getFileCache();
        if (mFileCache != null) {
            return mFileCache.getFile(mUrl);
        }
        return null;
    }

    @Override
    public void setBitmapSavedThread(Runnable thread) {
        mFileSaverRunnable = thread;
    }

    public Runnable getSaverRunnable(){
        return mFileSaverRunnable;
    }

    @Override
    public void setCurrentThread(Thread currentThread) {
        mCurrentThread = currentThread;
    }

    public void recycle() {
        // Releases references to the byte buffer and the BitMap
        if(mCurrentThread != null && !mCurrentThread.isInterrupted()){
            mCurrentThread.interrupt();
        }
        mCurrentThread = null;
        mBitmapBytes = null;
        mUrl = null;
    }
}
