package com.yousuf.image.downloader.threads;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.yousuf.image.downloader.ImageDownloadManager;
import com.yousuf.image.downloader.cache.FileCache;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Runnable that fetches the bitmap from internal storage or from network.
 */
public class BitmapLoader implements
        FileFetcherRunnable.TaskRunnableFileFetcherMethods,
        PhotoDownloadRunnable.TaskRunnableDownloadMethods,
        PhotoDecodeRunnable.TaskRunnableDecodeMethods {

    private int WIDTH = 450;

    private int HEIGHT = 450;

    private WeakReference<ImageView> imageReference;

    private String url;

    private byte[] mBitmapBytes;

    private Bitmap mDecodedImage;

    private Runnable mDownloadRunnable;

    private Runnable mDecodeRunnable;

    private Runnable mFileLoaderRunnable;

    private Thread mCurrentThread;

    private boolean mCached;

    public BitmapLoader() {
    }

    public void initBitmapLoadProcess(ImageView imgView, String imgUrl, boolean cached) {
        url = imgUrl;
        mCached = cached;

        WIDTH = imgView.getWidth() < WIDTH ? WIDTH : imgView.getWidth();
        HEIGHT = imgView.getHeight() < HEIGHT ? HEIGHT : imgView.getHeight();
        imageReference = new WeakReference<ImageView>(imgView);

        mFileLoaderRunnable = new FileFetcherRunnable(this);
        mDownloadRunnable = new PhotoDownloadRunnable(this);
        mDecodeRunnable = new PhotoDecodeRunnable(this);
    }

    @Override
    public void setCurrentThread(Thread currentThread) {
        mCurrentThread = currentThread;
    }

    @Override
    public void setImageDecodeThread(Runnable thread) {
        mDecodeRunnable = thread;
    }

    @Override
    public void setDownloadThread(Runnable thread) {
        mDownloadRunnable = thread;
    }

    @Override
    public void setFileFetcherThread(Runnable thread) {
        mFileLoaderRunnable = thread;
    }

    @Override
    public void handleDecodeState(int state) {
        ImageDownloadManager.getInstance().handleRequestStates(this, state);
    }

    @Override
    public int getTargetWidth() {
        return WIDTH;
    }

    @Override
    public int getTargetHeight() {
        return HEIGHT;
    }

    @Override
    public void setImage(Bitmap image) {
        mDecodedImage = image;
    }

    @Override
    public byte[] getByteBuffer() {
        return mBitmapBytes;
    }

    @Override
    public void setByteBuffer(byte[] buffer) {
        mBitmapBytes = buffer;
    }

    @Override
    public void handleDownloadState(int state) {
        ImageDownloadManager.getInstance().handleRequestStates(this, state);
    }

    @Override
    public URL getImageURL() {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Runnable getFileFetchRunnable() {
        return mFileLoaderRunnable;
    }

    public Runnable getPhotoDecodeRunnable() {
        return mDecodeRunnable;
    }

    public Runnable getDownloadRunnable() {
        return mDownloadRunnable;
    }

    public String getImageUrlString() {
        return url;
    }

    public Bitmap getImage() {
        return mDecodedImage;
    }

    public WeakReference<ImageView> getImageReference() {
        return imageReference;
    }

    public boolean isCacheEnabled(){
        return mCached;
    }

    public void recycle() {
        // Deletes the weak reference to the imageView
        if (null != imageReference) {
            imageReference.clear();
            imageReference = null;
        }

        // Releases references to the byte buffer and the BitMap
        if (mCurrentThread != null && !mCurrentThread.isInterrupted()) {
            mCurrentThread.interrupt();
        }

        mCurrentThread = null;
        mBitmapBytes = null;
        mDecodedImage = null;
        url = null;

    }

    @Override
    public File getFile() {
        FileCache mFileCache = ImageDownloadManager.getInstance().getFileCache();
        if (mFileCache != null) {
            return mFileCache.getFile(url);
        }
        return null;
    }

}