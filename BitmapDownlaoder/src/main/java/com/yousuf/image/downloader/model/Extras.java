package com.yousuf.image.downloader.model;

/**
 * Created by u471637 on 4/14/16.
 */
public class Extras {

    // Sets the size for each read action (bytes)
    public static final int READ_SIZE = 1024 * 2;

    // Limits the number of times the decoder tries to process an image
    public static final int NUMBER_OF_DECODE_TRIES = 2;

    // Tells the Runnable to pause for a certain number of milliseconds
    public static final long SLEEP_TIME_MILLISECONDS = 250;

    public static final int FAILED = -1;

    public static final int DOWNLOAD_STARTED = 1;

    public static final int DOWNLOAD_COMPLETE = 2;

    public static final int DECODE_STARTED = 3;

    public static final int TASK_COMPLETE = 4;

    public static final int FILE_FOUND = 5;

    public static final int FILE_FETCH_FAILED = 6;

    public static final int SAVE_PROCESS_COMPLETED = 7;

}
