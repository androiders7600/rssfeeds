package com.yousuf.image.downloader.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.yousuf.image.downloader.utils.DownloaderUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Yousuf Syed
 */
public class RestClientUtils {

    /**
     * Get Contents from url
     *
     * @param url url for contents.
     * @return contents as String
     * @throws IOException
     */
    public static String getContent(String url) throws IOException {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                return DownloaderUtils.getStringFromStream(urlConnection.getInputStream());
            }
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    /**
     * Get scaled down version of bitmap from url
     *
     * @param url image url
     * @return scaled down version of bitmap
     * @throws IOException
     */
    public static Bitmap getBitmap(String url) throws Exception {
        InputStream is;
        Bitmap bmp = null;
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();
                if (is != null) {
                    bmp = decodeFile(is);
                    is.close();
                }
            }
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return bmp;
    }

    /**
     * Get actual Bitmap
     *
     * @param url image url
     * @return Bitmap fetched from the url
     * @throws Exception
     */
    public static Bitmap getFeatureBitmap(String url) throws Exception {
        InputStream is;
        Bitmap bmp = null;
        HttpURLConnection urlConnection = null;
        Log.i("REST CLIENT", url);
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();
                if (is != null) {
                    bmp = BitmapFactory.decodeStream(is);
                    is.close();
                }
            }
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return bmp;
    }

    /**
     * Save bitmap to file.
     *
     * @param file file into which the bitmap should be saved
     * @param bmp bitmap to save
     * @throws IOException
     */
    public static void saveBitmapToFile(final File file, final Bitmap bmp) throws IOException {
        if (file == null || bmp == null) {
            return;
        }

        FileOutputStream fos = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.flush();
        fos.close();
    }



    /**
     * Decode input stream to bitmap
     * @param is input stream
     * @return scaled Bitmap
     * @throws Exception
     */
    public static Bitmap decodeFile(InputStream is) throws Exception {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = false;
        o.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, o);
        is.close();
        return bitmap;
    }

}
