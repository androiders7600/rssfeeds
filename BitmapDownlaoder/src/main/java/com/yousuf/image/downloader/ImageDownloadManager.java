package com.yousuf.image.downloader;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import com.yousuf.image.downloader.cache.FileCache;
import com.yousuf.image.downloader.model.Extras;
import com.yousuf.image.downloader.threads.BitmapLoader;
import com.yousuf.image.downloader.threads.DataSaver;
import com.yousuf.image.downloader.utils.DownloaderUtils;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yousuf Syed.
 */
public class ImageDownloadManager {

    private static ImageDownloadManager sInstance; // Singleton instance of the Bitmap Loader that provides cached bitmap or fetches one from the url if

    private long DEFAULT_CACHE_INTERVAL = 1 * 60 * 60 * 1000;

    // Sets the size of the storage that's used to cache images
    private static final int IMAGE_CACHE_SIZE = 1024 * 1024 * 4;

    private static final int IMAGE_ANIMATION = 300;

    private FileCache mFileCache;

    private Map<ImageView, String> mImageViewMap;

    private final LruCache<String, byte[]> mBitmapCache;

    private final BlockingQueue<Runnable> mDownloadWorkQueue;

    private final BlockingQueue<Runnable> mDecodeWorkQueue;

    private final BlockingQueue<Runnable> mFileSaverQueue;

    private final BlockingQueue<Runnable> mFileLoaderQueue;

    private Queue<BitmapLoader> mPhotoTaskWorkQueue;

    private Queue<DataSaver> mIOWorkQueue;

    private ThreadPoolExecutor mExecutorServicePool;

    private ThreadPoolExecutor mDecoderServicePool;

    private ThreadPoolExecutor mFileLoaderServicePool;

    private ThreadPoolExecutor mFileSaverServicePool;

    private static final int KEEP_ALIVE_TIME = 1;

    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    private static final int CORE_POOL_SIZE = 8;

    private static final int MAXIMUM_POOL_SIZE = 8;

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    // A static block that sets class fields
    static {
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
        sInstance = new ImageDownloadManager();
    }

    /**
     * Initialize the cache and executor instances.
     * Generally, init from Application instance.
     *
     * @param ctx
     */
    public void init(Context ctx) {
        // make sure that we init only once.
        if (mImageViewMap == null) {
            mImageViewMap = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());

            try {
                mFileCache = new FileCache(ctx);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Singleton instance to the Download Manager
     *
     * @return instance of DownloadManager
     */
    public static ImageDownloadManager getInstance() {
        return sInstance;
    }

    /**
     * Set cache policy, default is 24 hours.
     *
     * @param millis time interval in milliseconds.
     */
    public void setCache(long millis) {
        DEFAULT_CACHE_INTERVAL = millis;
    }

    private ImageDownloadManager() {

        mBitmapCache = new LruCache<String, byte[]>(IMAGE_CACHE_SIZE);

        mFileLoaderQueue = new LinkedBlockingQueue<Runnable>();

        mDownloadWorkQueue = new LinkedBlockingQueue<Runnable>();

        mDecodeWorkQueue = new LinkedBlockingQueue<Runnable>();

        mFileSaverQueue = new LinkedBlockingQueue<Runnable>();

        mPhotoTaskWorkQueue = new LinkedBlockingQueue<BitmapLoader>();

        mIOWorkQueue = new LinkedBlockingQueue<DataSaver>();

        mFileLoaderServicePool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mFileLoaderQueue);

        mExecutorServicePool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mDownloadWorkQueue);

        mDecoderServicePool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mDecodeWorkQueue);

        mFileSaverServicePool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mFileSaverQueue);

    }

    @SuppressWarnings("HandlerLeak")
    private Handler mHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message inputMessage) {

            Object handleObject = inputMessage.obj;

            if (handleObject instanceof DataSaver) {
                DataSaver saver = (DataSaver) handleObject;
                if (inputMessage.what == Extras.SAVE_PROCESS_COMPLETED) {
                    recycleSaverTask(saver);
                }
            } else {
                // Gets the image task from the incoming Message object.
                BitmapLoader bitmapLoader = (BitmapLoader) handleObject;

                // Sets an PhotoView that's a weak reference to the
                // input ImageView
                WeakReference<ImageView> imageReference = bitmapLoader.getImageReference();

                ImageView localView = null;

                if (imageReference != null && (localView = imageReference.get()) != null) {

                    String localURL = bitmapLoader.getImageUrlString();

                    if (!isImageViewReused(imageReference, localURL)) {

                        switch (inputMessage.what) {

                            case Extras.DOWNLOAD_STARTED:
                                //localView.setImageDrawable(null);
                                break;

                            // If the download is complete, but the decode is waiting,
                            case Extras.DOWNLOAD_COMPLETE:
                                localView.setImageDrawable(null);
                                break;

                            case Extras.DECODE_STARTED:
                                localView.setImageDrawable(null);
                                break;

                            case Extras.TASK_COMPLETE:
                                localView.setImageBitmap(bitmapLoader.getImage());
                                ObjectAnimator imgAnim = ObjectAnimator.ofInt(localView, "alpha", 0, 255);
                                imgAnim.setDuration(IMAGE_ANIMATION);
                                imgAnim.start();
                                recycleLoaderTask(bitmapLoader);
                                break;

                            // The download failed, sets the background color to dark red
                            case Extras.FAILED:
                                localView.setImageResource(R.drawable.ic_failed);
                                ObjectAnimator imgFailed = ObjectAnimator.ofInt(localView, "alpha", 0, 255);
                                imgFailed.setDuration(IMAGE_ANIMATION);
                                imgFailed.start();
                                recycleLoaderTask(bitmapLoader);
                                break;

                            default:
                                super.handleMessage(inputMessage);
                                break;
                        }
                    }
                }
            }
        }
    };

    /**
     * Recycles tasks by calling their internal recycle() method and then putting them back into
     * the task queue.
     *
     * @param downloadTask The task to recycle
     */
    void recycleLoaderTask(BitmapLoader downloadTask) {
        downloadTask.recycle();                     // Frees up memory in the task
        mPhotoTaskWorkQueue.offer(downloadTask);    // Puts the task object back into the queue for re-use.
    }

    /**
     * Recycles tasks by calling their internal recycle() method and then putting them back into
     * the task queue.
     *
     * @param dataSaver The task to recycle
     */
    void recycleSaverTask(DataSaver dataSaver) {
        dataSaver.recycle();              // Frees up memory in the task
        mIOWorkQueue.offer(dataSaver);    // Puts the task object back into the queue for re-use.
    }

    /**
     * @param loader
     * @param state
     */
    public void handleRequestStates(BitmapLoader loader, int state) {

        switch (state) {
            case Extras.FILE_FETCH_FAILED:
                mExecutorServicePool.execute(loader.getDownloadRunnable());
                mHandler.obtainMessage(state, loader).sendToTarget();
                break;

            case Extras.TASK_COMPLETE:
                if(loader.isCacheEnabled()) {
                    mBitmapCache.put(loader.getImageUrlString(), loader.getByteBuffer());
                }
                mHandler.obtainMessage(state, loader).sendToTarget();
                break;

            case Extras.FILE_FOUND:
            case Extras.DOWNLOAD_COMPLETE:
                mDecoderServicePool.execute(loader.getPhotoDecodeRunnable());
                saveFile(loader.getByteBuffer(), loader.getImageUrlString());
                mHandler.obtainMessage(state, loader).sendToTarget();
                break;

            default:
                mHandler.obtainMessage(state, loader).sendToTarget();
        }
    }

    /**
     * @param dataSaver
     * @param state
     */
    public void handleSaveRequestStates(DataSaver dataSaver, int state) {
        mHandler.obtainMessage(state, dataSaver).sendToTarget();
    }

    /**
     * @return
     */
    public FileCache getFileCache() {
        return mFileCache;
    }

    /**
     * Load ImageView with either a cacheEnabled bitmap
     * or download from network if no cache is available.
     *
     * @param thumbnail image view on which bitmap is loaded.
     */
    public void loadBitmap(ImageView thumbnail, String imageUrl, boolean cacheEnabled) {
        if (thumbnail != null && !TextUtils.isEmpty(imageUrl)) {
            mImageViewMap.put(thumbnail, imageUrl);
            if (cacheEnabled) {
                getCachedBitmapLoader(thumbnail, imageUrl, cacheEnabled);
            } else {
                getFeaturedBitmapLoader(thumbnail, imageUrl, cacheEnabled);
            }
        }
    }

    /**
     *
     * @param buffer
     * @param url
     */
    public void saveFile(byte[] buffer, String url) {

        DataSaver dataSaver = sInstance.mIOWorkQueue.poll();

        // If the queue was empty, create a new task instead.
        if (null == dataSaver) {
            dataSaver = new DataSaver();
        }

        dataSaver.initDataSaver(buffer, url);
        mFileLoaderServicePool.execute(dataSaver.getSaverRunnable());
    }

    /**
     * Get a new Bitmap each time the request is made.
     *
     * @param thumbnail
     * @param imageUrl
     */
    public void getFeaturedBitmapLoader(ImageView thumbnail, String imageUrl, boolean cacheEnabled) {

        BitmapLoader bitmapLoader = sInstance.mPhotoTaskWorkQueue.poll();

        // If the queue was empty, create a new task instead.
        if (null == bitmapLoader) {
            bitmapLoader = new BitmapLoader();
        }

        bitmapLoader.initBitmapLoadProcess(thumbnail, imageUrl, cacheEnabled);
        mFileLoaderServicePool.execute(bitmapLoader.getFileFetchRunnable());
        thumbnail.setImageResource(R.drawable.ic_default);
    }

    /**
     * Get a cached Bitmap if bitmap cache is enabled.
     *
     * @param thumbnail
     * @param imageUrl
     */
    public void getCachedBitmapLoader(ImageView thumbnail, String imageUrl, boolean cacheEnabled) {
    /*
     * Gets a task from the pool of tasks, returning null if the pool is empty
        */
        BitmapLoader bitmapLoader = sInstance.mPhotoTaskWorkQueue.poll();

        // If the queue was empty, create a new task instead.
        if (null == bitmapLoader) {
            bitmapLoader = new BitmapLoader();
        }

        bitmapLoader.initBitmapLoadProcess(thumbnail, imageUrl, cacheEnabled);
        bitmapLoader.setByteBuffer(sInstance.mBitmapCache.get(bitmapLoader.getImageUrlString()));

        // If the byte buffer was empty, the image wasn't cacheEnabled
        if (null == bitmapLoader.getByteBuffer()) {
            mFileLoaderServicePool.execute(bitmapLoader.getFileFetchRunnable());
            thumbnail.setImageResource(R.drawable.ic_default);
        } else {
            handleRequestStates(bitmapLoader, Extras.DOWNLOAD_COMPLETE);
        }
    }

    /**
     * Check whether the image view was reused to display another image.
     *
     * @param thumbnailReference weak reference to the thumbnail
     * @param url                imageUrl associated with the ImageView.
     * @return true if the imageview is reused, false otherwise.
     */
    private boolean isImageViewReused(WeakReference<ImageView> thumbnailReference, String url) {
        if (thumbnailReference != null && !TextUtils.isEmpty(url)) {
            ImageView thumbnail = thumbnailReference.get();
            if (thumbnail != null) {
                String tag = mImageViewMap.get(thumbnail);
                if (tag == null || !tag.equals(url)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Clear cache when exiting the activity.
     * All though the clear happens if the cache expired which is currently set to 1 hour.
     * You can change the Cache interval through setCache(int milles)
     *
     * @param ctx application context
     */
    public void clearCache(Context ctx) {
        if (DownloaderUtils.isCacheExpired(ctx, DEFAULT_CACHE_INTERVAL)) {

            if (mBitmapCache != null) {
                mBitmapCache.evictAll();
            }

            if (mImageViewMap != null) {
                mImageViewMap.clear();
            }

            if (mFileCache != null) {
                mFileCache.clear();
            }
        }
    }

}

