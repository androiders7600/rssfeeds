package com.yousuf.image.downloader.listeners;

import android.graphics.Bitmap;

/**
 * Created by u471637 on 3/9/16.
 */
public interface BitmapResponseListener {
    void onBitmapReceived(Bitmap bmp);
}
