package com.yousuf.image.downloader.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by u471637 on 2/15/16.
 */
public class FileCache {

    private File mCacheDir;

    public FileCache(Context ctx) throws FileNotFoundException {
        // mCacheDir = new File(ctx.getFilesDir(), "/data/RssFeeds");

        mCacheDir = getFeedsStorageDir(ctx, "/BitmapDownloads");
        if (mCacheDir == null) {
            mCacheDir = new File(ctx.getFilesDir(), "/data/BitmapDownloads");
        }
        if (!mCacheDir.exists())
            mCacheDir.mkdirs();

    }

    public File getFile(String url) {
        String fileName = String.valueOf(url.hashCode()) + ".png";
        return new File(mCacheDir, fileName);
    }

    public void clear() {
        (new Runnable() {

            @Override
            public void run() {
                File[] files = mCacheDir.listFiles();
                if (files != null) {
                    for (File f : files) {
                        if (f.exists()) {
                            f.delete();
                        }
                    }
                }
            }
        }).run();

    }

    // Get the directory for the app's private pictures directory.
    public File getFeedsStorageDir(Context context, String FeedsName) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), FeedsName);
        if (!file.mkdirs()) {
            Log.e("File Cache", "Feeds Directory not created");
        }
        return file;
    }

    /**
     * Decode file to bitmap
     *
     * @param file file
     * @return Decoded Bitmap
     */
    public Bitmap decodeFile(File file) {
        if(!file.exists()){
            return null;
        }
        Bitmap bitmap = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            bitmap = BitmapFactory.decodeStream(fis);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Save bitmap to file.
     *
     * @param file file into which the bitmap should be saved
     * @param bmp  bitmap to save
     * @throws IOException
     */
    public void saveBitmapToFile(File file, final Bitmap bmp) throws IOException {
        if (file == null || bmp == null) {
            return;
        }

        FileOutputStream fos = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.flush();
        fos.close();
    }

}
