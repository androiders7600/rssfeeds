package com.yousuf.image.downloader.utils;

import android.content.Context;
import android.content.SharedPreferences;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yousuf Syed
 */
public class DownloaderUtils {

    private static final String LAST_CACHE_INTERVAL = "last-cache-timestamp";

    /**
     * Check if device is connected to internet.
     *
     * @param ctx app context
     * @return true if connected, false otherwise.
     */
    public static boolean isConnectedToNetwork(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

    /**
     * Convert InputStream Response into String
     *
     * @param is input stream response
     * @return Response string.
     * @throws IOException
     */
    public static String getStringFromStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String nextLine = "";

        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }

    /**
     * Check if cache have expired
     *
     * @param ctx app context
     * @return true if expired, false otherwise.
     */
    public static boolean isCacheExpired(Context ctx, long duration) {
        SharedPreferences preferences = ctx.getSharedPreferences("com.sample.image.downloader", Context.MODE_PRIVATE);
        long current = System.currentTimeMillis();
        long lastCache = preferences.getLong(LAST_CACHE_INTERVAL, 0);

        if (current - lastCache > duration) {
            preferences.edit().putLong(LAST_CACHE_INTERVAL, current).apply();
            return true;
        }
        return false;
    }

}
