package com.yousuf.image.downloader.threads;

import android.util.Log;

import com.yousuf.image.downloader.model.Extras;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ysyed2 on 2/27/16.
 */
public class BitmapSaverRunnable implements Runnable {

    public interface TaskRunnableBitmapSaverMethods {

        void handleSaveState(int state);

        byte[] getImageBuffer();

        File getFile();

        void setBitmapSavedThread(Runnable thread);

        void setCurrentThread(Thread thread);
    }

    final TaskRunnableBitmapSaverMethods mBitmapLoader;

    public BitmapSaverRunnable(TaskRunnableBitmapSaverMethods loader) {
        mBitmapLoader = loader;
    }

    @Override
    public void run() {
        mBitmapLoader.setCurrentThread(Thread.currentThread());

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        try {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            File file = mBitmapLoader.getFile();
            byte[] buffer = mBitmapLoader.getImageBuffer();
            saveBitmapToFile(file, buffer);

            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

        } catch (IOException | InterruptedException e) {
            if(e.getMessage() != null) {
                Log.d("Photo_SAVER", e.getMessage());
            }
            e.printStackTrace();
        } finally {
            java.lang.System.gc();
            //To recycle existing objects.
            mBitmapLoader.handleSaveState(Extras.SAVE_PROCESS_COMPLETED);
            mBitmapLoader.setBitmapSavedThread(null);
            Thread.interrupted();
        }
    }

    /**
     * Save bitmap to file.
     *
     * @param file   file into which the bitmap should be saved
     * @param buffer byte[] to save
     * @throws IOException
     */
    public void saveBitmapToFile(File file, byte[] buffer) throws IOException {
        if (file == null || buffer == null) {
            return;
        }

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(buffer);
        fos.flush();
        fos.close();
    }
}

