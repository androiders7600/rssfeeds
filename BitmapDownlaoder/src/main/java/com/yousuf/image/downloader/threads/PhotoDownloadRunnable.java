/*
 * Copyright (C) ${year} The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yousuf.image.downloader.threads;

import android.util.Log;

import com.yousuf.image.downloader.model.Extras;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PhotoDownloadRunnable implements Runnable {

    public interface TaskRunnableDownloadMethods {

        void setDownloadThread(Runnable currentThread);

        void setByteBuffer(byte[] buffer);

        void handleDownloadState(int state);

        URL getImageURL();
    }

    // Defines a field that contains the calling object of type PhotoTask.
    final TaskRunnableDownloadMethods mPhotoTask;

    public PhotoDownloadRunnable(TaskRunnableDownloadMethods photoTask) {
        mPhotoTask = photoTask;
    }


    @SuppressWarnings("resource")
    @Override
    public void run() {

        mPhotoTask.setDownloadThread(Thread.currentThread());

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        byte[] byteBuffer = null;

        try {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            mPhotoTask.handleDownloadState(Extras.DOWNLOAD_STARTED);

            InputStream byteStream = null;

            try {

                HttpURLConnection httpConn = (HttpURLConnection) mPhotoTask.getImageURL().openConnection();

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

                byteStream = httpConn.getInputStream();

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

                int contentSize = httpConn.getContentLength();

                if (-1 == contentSize) {

                    byte[] tempBuffer = new byte[Extras.READ_SIZE];
                    int bufferLeft = tempBuffer.length;
                    int bufferOffset = 0;
                    int readResult = 0;

                    outer:
                    do {
                        while (bufferLeft > 0) {
                            readResult = byteStream.read(tempBuffer, bufferOffset, bufferLeft);

                            if (readResult < 0) {
                                break outer;
                            }

                            bufferOffset += readResult;
                            bufferLeft -= readResult;
                            if (Thread.interrupted()) {
                                throw new InterruptedException();
                            }
                        }

                        bufferLeft = Extras.READ_SIZE;
                        int newSize = tempBuffer.length + Extras.READ_SIZE;
                        byte[] expandedBuffer = new byte[newSize];
                        System.arraycopy(tempBuffer, 0, expandedBuffer, 0, tempBuffer.length);
                        tempBuffer = expandedBuffer;
                    } while (true);

                    byteBuffer = new byte[bufferOffset];

                    System.arraycopy(tempBuffer, 0, byteBuffer, 0, bufferOffset);

                } else {

                    byteBuffer = new byte[contentSize];
                    int remainingLength = contentSize;
                    int bufferOffset = 0;

                    while (remainingLength > 0) {
                        int readResult = byteStream.read(byteBuffer, bufferOffset, remainingLength);

                        if (readResult < 0) {
                            throw new EOFException();
                        }

                        bufferOffset += readResult;
                        remainingLength -= readResult;

                        if (Thread.interrupted()) {
                            throw new InterruptedException();
                        }
                    }
                }

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

            } catch (IOException e) {
                if(e.getMessage() != null) {
                    Log.d("PhotoDownload", e.getMessage());
                }
                e.printStackTrace();
                return;
            } finally {
                if (null != byteStream) {
                    try {
                        byteStream.close();
                    } catch (Exception e) {

                    }
                }
            }


            mPhotoTask.setByteBuffer(byteBuffer);
            mPhotoTask.handleDownloadState(Extras.DOWNLOAD_COMPLETE);
        } catch (InterruptedException e1) {
            Log.d("PhotoDownload", e1.getMessage());
        } finally {
            // If the byteBuffer is null, reports that the download failed.
            if (null == byteBuffer) {
                mPhotoTask.handleDownloadState(Extras.FAILED);
            }

            mPhotoTask.setDownloadThread(null);
            Thread.interrupted();
        }
    }
}

