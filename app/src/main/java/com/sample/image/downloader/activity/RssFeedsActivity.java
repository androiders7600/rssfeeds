package com.sample.image.downloader.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.sample.image.downloader.R;
import com.sample.image.downloader.fragment.FeedDetailsFragment;
import com.sample.image.downloader.fragment.FeedListFragment;
import com.sample.image.downloader.model.CategoryData;
import com.sample.image.downloader.model.Extras;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.utils.NetworkUtils;
import com.yousuf.image.downloader.ImageDownloadManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RssFeedsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private boolean mSavedInstance;

    private boolean mMultiView;

    private DrawerLayout mDrawerLayout;

    private NavigationView mNavigationView;

    /**
     * Setup Toolbar to display as Action bar.
     */
    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.app_name);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setUpNavigationView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        setUpNavigationView();

        if (savedInstanceState != null) {
            mSavedInstance = true;
        }
        mMultiView = (findViewById(R.id.feeds_details_container) != null);
        loadCategories();
        loadFeedsData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        ImageDownloadManager.getInstance().clearCache(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if ((!mMultiView && (getFragmentManager().getBackStackEntryCount() > 1))) {
            getFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            if (intent.getAction().equals(Extras.ACTION_FEED_DETAILS)) {
                FeedsData data = intent.getParcelableExtra(Extras.EXTRA_FEEDS_DATA);
                showFeedDetails(data);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(Extras.ACTION_FEED_DETAILS);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mReceiver, intentFilter);

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mReceiver);
        super.onPause();
    }

    private void loadFeedsData() {
        displayFeedsList();
        if (mMultiView) {
            showFeedDetails(null);
        }
    }

    public void displayFeedsList() {
        FragmentManager fm = getSupportFragmentManager();
        if (!mSavedInstance) {
            getFragmentManager().popBackStack();
            FeedListFragment listFragment = new FeedListFragment();
            fm.beginTransaction().replace(R.id.fragment_container, listFragment, FeedListFragment.TAG).commit();
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof FeedDetailsFragment) {
                fm.beginTransaction().remove(fragment).commit();
                fm.popBackStack();
            }
        }
    }

    private void showFeedDetails(FeedsData data) {
        FeedDetailsFragment detailFragment;
        if (mMultiView) {
            detailFragment = (FeedDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.feeds_details_container);
            if (detailFragment == null) {
                detailFragment = new FeedDetailsFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.feeds_details_container, detailFragment, FeedDetailsFragment.TAG).commit();
            } else {
                detailFragment.updateFeedDetails(data);
            }
        } else {
            detailFragment = new FeedDetailsFragment();
            detailFragment.setFeedDetails(data);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, detailFragment, FeedDetailsFragment.TAG);
            ft.addToBackStack(FeedDetailsFragment.TAG).commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    public class FeedCategoryDownloaderTask extends AsyncTask<String, String, ArrayList<CategoryData>> {

        @Override
        protected ArrayList<CategoryData> doInBackground(String... params) {
            try {
                return NetworkUtils.getCategories(params[0]);
            } catch (Exception io) {
                Log.i("RssFeedDownloaderTask", "Issue with IO");
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<CategoryData> data) {
            super.onPostExecute(data);
            if (!isCancelled()) {
                displayNavigationItems(data);
            }
        }
    }

    private void displayNavigationItems(List<CategoryData> data) {
        if(data != null) {
            Menu menu = mNavigationView.getMenu();
            SubMenu subMenu = menu.addSubMenu("Categories");
            Iterator<CategoryData> iterator = data.iterator();
            CategoryData categoryData;

            while (iterator.hasNext()) {
                categoryData = iterator.next();
                subMenu.add(categoryData.getName());
            }
        }
    }

    private void loadCategories() {
        FeedCategoryDownloaderTask categoryDownloaderTask = new FeedCategoryDownloaderTask();
        categoryDownloaderTask.execute(Extras.FEED_CATEGORIES);
    }

    public void showToast(int resId){
        Snackbar.make(mDrawerLayout, getString(resId), Snackbar.LENGTH_LONG).show();
    }
}
