package com.sample.image.downloader.fragment;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sample.image.downloader.R;
import com.sample.image.downloader.model.Extras;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.utils.AppUtils;
import com.sample.image.downloader.views.RoundedCornerImageView;
import com.yousuf.image.downloader.ImageDownloadManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedDetailsFragment extends Fragment {

    public static final String TAG = FeedDetailsFragment.class.getSimpleName();

    private RoundedCornerImageView mImageView;

    private TextView mTitle, mCategories, mTime, mSummary, mAuthor;

    public FeedDetailsFragment() {
        // Required empty public constructor
    }

    private FeedsData mFeedDetails;

    private void initViews(View root) {
        mImageView = (RoundedCornerImageView) root.findViewById(R.id.image_thumbnail);
        mTitle = (TextView) root.findViewById(R.id.textView_title);
        mCategories = (TextView) root.findViewById(R.id.textView_categories);
        mTime = (TextView) root.findViewById(R.id.textView_date);
        mSummary = (TextView) root.findViewById(R.id.textView_summary);
        mAuthor = (TextView) root.findViewById(R.id.textView_author);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_feed_details, container, false);
        if (savedInstanceState != null) {
            mFeedDetails = savedInstanceState.getParcelable(Extras.EXTRA_FEED_DETAILS);
        }
        initViews(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            FeedsData data = getArguments().getParcelable(Extras.EXTRA_FEED_DETAILS);
            updateFeedDetails(data);
        } else if (mFeedDetails != null) {
            updateFeedDetails(mFeedDetails);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Extras.EXTRA_FEED_DETAILS, mFeedDetails);
    }

    public void setFeedDetails(FeedsData data) {
        if (data != null) {
            mFeedDetails = data;
        }
    }

    //private FeatureImageDownloaderTask task = null;

    public void updateFeedDetails(FeedsData data) {
        if (data != null) {
            mFeedDetails = data;
            mTime.setText(AppUtils.getDate(getActivity(), data.getPostDate()));
            mTitle.setText(Html.fromHtml(data.getPostTitle()));
            mAuthor.setText(data.getPostAuthor());
            String content = data.getPostSnippet().replaceAll("\n", " ").replaceAll("\t", " ");
            mSummary.setText(Html.fromHtml(content));
            mCategories.setText(data.getPostCategories());

            String url = data.getPostUrl();
            if (!TextUtils.isEmpty(url) ) {
                ImageDownloadManager.getInstance().loadBitmap(mImageView, url, false);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
