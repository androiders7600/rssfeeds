package com.sample.image.downloader.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.image.downloader.R;
import com.sample.image.downloader.model.Extras;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.utils.AppUtils;
import com.yousuf.image.downloader.ImageDownloadManager;

import java.util.List;

/**
 * Created by Yousuf Syed
 */
public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.ItemHolder> {

    private List<FeedsData> mFeedsDataList;

    private Context mContext;

    public FeedsAdapter(Context ctx, List<FeedsData> feedsDataList) {
        mContext = ctx;
        mFeedsDataList = feedsDataList;
    }

    public void addFeedsList(List<FeedsData> feedsDataList){
        mFeedsDataList = feedsDataList;
        notifyDataSetChanged();
    }

    public List<FeedsData> getFeedList(){
        return mFeedsDataList;
    }

    @Override
    public int getItemCount() {
        return mFeedsDataList.size();
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int pos) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_item, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        FeedsData data = mFeedsDataList.get(position);
        holder.mDate.setText(AppUtils.getDate(mContext, data.getPostDate()));
        holder.mTitle.setText(Html.fromHtml(data.getPostTitle()));
        holder.mAuthor.setText(data.getPostAuthor());
        String content = data.getPostSnippet().replaceAll("\n", " ").replaceAll("\t", " ");
        holder.mSummary.setText(Html.fromHtml(content));
        holder.mCategories.setText(data.getPostCategories());
        holder.mThumbnail.setImageResource(R.drawable.ic_default);

//        ImageRequest request = (new ImageRequest.ImageRequestBuilder(mContext)
//                .setImage(holder.mThumbnail)
//                .setUrl(data.getPostThumbnailUrl())
//                .setScaleDown(true)).build();

        ImageDownloadManager.getInstance().loadBitmap(holder.mThumbnail, data.getPostThumbnailUrl(), true);

    }

    public class ItemHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitle, mAuthor, mSummary, mCategories, mDate;

        private ImageView mThumbnail;

        public ItemHolder(View root) {
            super(root);
            root.setOnClickListener(this);
            mTitle = (TextView) root.findViewById(R.id.textView_title);
            mAuthor = (TextView) root.findViewById(R.id.textView_author);
            mSummary = (TextView) root.findViewById(R.id.textView_summary);
            mDate = (TextView) root.findViewById(R.id.textView_date);
            mCategories = (TextView) root.findViewById(R.id.textView_categories);
            mThumbnail = (ImageView) root.findViewById(R.id.image_thumbnail);

            ((CardView) root).setCardBackgroundColor(Color.WHITE);
            updateCardPadding(root);
        }

        /**
         * This code is to add padding to card view for API version 21 or higher as
         * we are using support library, without which the cards line up
         * without any padding one below the other on API >= 21.
         */
        protected void updateCardPadding(View root) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((CardView) root).setUseCompatPadding(true);
            }
        }

        @Override
        public void onClick(View v) {
            FeedsData data = mFeedsDataList.get(getLayoutPosition());
            Intent intent = new Intent(Extras.ACTION_FEED_DETAILS);
            intent.putExtra(Extras.EXTRA_FEEDS_DATA, data);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
