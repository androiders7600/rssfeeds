package com.sample.image.downloader.model;

/**
 * Created by Yousuf Syed
 */
public class Posts {

    public static final String ROOT = "posts";

    public static final String POST_ID = "ID";

    public static final String AUTHOR = "author";

    public static final String AUTHOR_NAME = "name";

    public static final String POST_DATE = "date";

    public static final String TITLE = "title";

    public static final String CONTENT = "content";

    public static final String POST_THUMBNAILS = "thumbnails";

    public static final String ALTERNATE_POST_THUMBNAIL = "post_thumbnail";

    public static final String ALTERNATE_THUMBNAIL_URL = "URL";

    public static final String ATTACHMENTS = "attachments";

    public static final String THUMBNAIL_URL = "thumbnail";

    public static final String FEATURE_URL = "large";

    public static final String CATEGORIES = "categories";

    public static final String CATEGORY_NAME = "name";

}
