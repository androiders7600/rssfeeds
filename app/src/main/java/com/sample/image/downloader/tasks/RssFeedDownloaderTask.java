package com.sample.image.downloader.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.sample.image.downloader.model.Extras;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.utils.NetworkUtils;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Yousuf Syed
 */
public class RssFeedDownloaderTask extends AsyncTask<String, String, ArrayList<FeedsData>> {

    private Context mContext;

    public RssFeedDownloaderTask(Context ctx) {
        mContext = ctx;
    }

    @Override
    protected ArrayList<FeedsData> doInBackground(String... params) {
        try {
            return NetworkUtils.getFeeds(params[0]);
        } catch (IOException io) {
            Log.i("RssFeedDownloaderTask", "Issue with IO");
        } catch (JSONException json) {
            Log.i("RssFeedDownloaderTask", "Error parsing JSON data");
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<FeedsData> data) {
        super.onPostExecute(data);
        Intent intent = new Intent(Extras.ACTION_UPDATE_FEED_LIST);
        intent.putParcelableArrayListExtra(Extras.EXTRA_FEEDS_DATA, data);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
}
