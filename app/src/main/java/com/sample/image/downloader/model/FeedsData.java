package com.sample.image.downloader.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Yousuf Syed
 */
public class FeedsData implements Parcelable {

    private String mPostDate;

    private String mPostAuthor;

    private String mPostSnippet;

    private String mPostThumbnailUrl;

    private String mPostUrl;

    private String mPostTitle;

    private String mPostId;

    private String mPostCategories;

    public String getPostDate() {
        return mPostDate;
    }

    public String getPostCategories() {
        return mPostCategories;
    }

    public String getPostAuthor() {
        return mPostAuthor;
    }

    public String getPostSnippet() {
        return mPostSnippet;
    }

    public String getPostThumbnailUrl() {
        return mPostThumbnailUrl;
    }

    public String getPostUrl() {
        return mPostUrl;
    }

    public String getPostTitle() {
        return mPostTitle;
    }

    public String getPostId() {
        return mPostId;
    }

    public void setPostDate(String date) {
        mPostDate = (date == null || date.equalsIgnoreCase("null")) ? "" : date;
    }

    public void setPostCategories(String categories) {
        mPostCategories = (categories == null || categories.equalsIgnoreCase("null")) ? "" : categories;
    }

    public void setPostAuthor(String author) {
        mPostAuthor = (author == null || author.equalsIgnoreCase("null")) ? "" : author;
    }

    public void setPostSnippet(String content) {
        mPostSnippet = (content == null || content.equalsIgnoreCase("null")) ? "" : content;
    }

    public void setPostThumbnailUrl(String thumbnailUrl) {
        mPostThumbnailUrl = (thumbnailUrl == null || thumbnailUrl.equalsIgnoreCase("null")) ? "" : thumbnailUrl;
    }

    public void setPostUrl(String url) {
        mPostUrl = (url == null || url.equalsIgnoreCase("null")) ? "" : url;
    }

    public void setPostTitle(String title) {
        mPostTitle = (title == null || title.equalsIgnoreCase("null")) ? "" : title;
    }

    public void setPostId(String id) {
        mPostId = (id == null || id.equalsIgnoreCase("null")) ? "" : id;
    }

    public FeedsData(JSONObject postJson) {
        parsePostData(postJson);
    }

    private void parsePostData(JSONObject postJson) {
        if (postJson != null) {
            setPostId(postJson.optString(Posts.POST_ID));
            setPostDate(postJson.optString(Posts.POST_DATE));
            setPostTitle(postJson.optString(Posts.TITLE));
            setPostSnippet(postJson.optString(Posts.CONTENT));

            parseAuthorJson(postJson.optJSONObject(Posts.AUTHOR));

            parseThumbnailJson(postJson.optJSONObject(Posts.ALTERNATE_POST_THUMBNAIL));
            if(TextUtils.isEmpty(mPostThumbnailUrl)){
                parseAlternateThumbnail(postJson.optJSONObject(Posts.ATTACHMENTS));
            } else {
                parseFeatureImage(postJson.optJSONObject(Posts.ATTACHMENTS));
            }


            parseCategoriesJson(postJson.optJSONObject(Posts.CATEGORIES));

        }
    }

    private void parseAuthorJson(JSONObject authorJson) {
        if (authorJson != null) {
            setPostAuthor(authorJson.optString(Posts.AUTHOR_NAME));
        }
    }

    private void parseThumbnailJson(JSONObject thumbnailJson) {
        if (thumbnailJson != null) {
            setPostThumbnailUrl(thumbnailJson.optString(Posts.ALTERNATE_THUMBNAIL_URL));
        }
    }

    private void parseAlternateThumbnail(JSONObject attachments){
        Iterator<String> list = attachments.keys();
        if(list.hasNext()) {
            JSONObject jsonObject = attachments.optJSONObject(list.next());
            if(jsonObject != null){
                JSONObject thumbnailsData = jsonObject.optJSONObject(Posts.POST_THUMBNAILS);
                if(thumbnailsData != null) {
                    setPostThumbnailUrl(thumbnailsData.optString(Posts.THUMBNAIL_URL));
                    setPostUrl(thumbnailsData.optString(Posts.FEATURE_URL));
                }
            }
        }

        if(TextUtils.isEmpty(mPostUrl)){
            setPostUrl(mPostThumbnailUrl);
        }
    }

    private void parseFeatureImage(JSONObject attachments){
        if(attachments == null) return;

        Iterator<String> list = attachments.keys();
        if(list.hasNext()) {
            JSONObject jsonObject = attachments.optJSONObject(list.next());
            if(jsonObject != null){
                JSONObject thumbnailsData = jsonObject.optJSONObject(Posts.POST_THUMBNAILS);
                if(thumbnailsData != null) {
                    setPostUrl(thumbnailsData.optString(Posts.FEATURE_URL));
                }
            }
        }

        if(TextUtils.isEmpty(mPostUrl)){
            setPostUrl(mPostThumbnailUrl);
        }
    }

    private void parseCategoriesJson(JSONObject categoryJson) {
        if (categoryJson != null) {
            StringBuilder sb = new StringBuilder();
            Iterator<String> catagoryIterator = categoryJson.keys();
            while(catagoryIterator.hasNext()){
                addCategory(sb, categoryJson.optJSONObject(catagoryIterator.next()));
            }
            setPostCategories(sb.toString());
        }
    }

    private void addCategory(StringBuilder sb, JSONObject categoryJson) {
        if (categoryJson != null) {
            if (!sb.toString().isEmpty()) {
                sb.append(", ");
            }
            sb.append(categoryJson.optString(Posts.CATEGORY_NAME));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public FeedsData(Parcel in) {
        mPostDate = in.readString();
        mPostAuthor = in.readString();
        mPostSnippet = in.readString();
        mPostThumbnailUrl = in.readString();
        mPostUrl = in.readString();
        mPostTitle = in.readString();
        mPostId = in.readString();
        mPostCategories = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPostDate);
        dest.writeString(mPostAuthor);
        dest.writeString(mPostSnippet);
        dest.writeString(mPostThumbnailUrl);
        dest.writeString(mPostUrl);
        dest.writeString(mPostTitle);
        dest.writeString(mPostId);
        dest.writeString(mPostCategories);
    }

    public static final Parcelable.Creator<FeedsData> CREATOR = new Creator<FeedsData>() {

        @Override
        public FeedsData createFromParcel(Parcel parcel) {
            return new FeedsData(parcel);
        }

        @Override
        public FeedsData[] newArray(int size) {
            return new FeedsData[size];
        }

    };

}
