package com.sample.image.downloader;

import android.app.Application;

import com.yousuf.image.downloader.ImageDownloadManager;

/**
 * Created by ysyed2 on 2/24/16.
 */
public class RssFeedsApplication extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        ImageDownloadManager.getInstance().init(this);
    }
}
