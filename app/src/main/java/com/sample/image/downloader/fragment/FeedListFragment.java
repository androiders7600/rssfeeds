package com.sample.image.downloader.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sample.image.downloader.R;
import com.sample.image.downloader.adapter.FeedsAdapter;
import com.sample.image.downloader.model.Extras;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.utils.AppUtils;
import com.sample.image.downloader.utils.NetworkUtils;
import com.yousuf.image.downloader.ImageDownloadManager;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Fragment to display list of feeds
 */
public class FeedListFragment extends Fragment {

    public static final String TAG = FeedListFragment.class.getSimpleName();

    private SwipeRefreshLayout mRefreshFeedsList;

    private RecyclerView mFeedRecycler;

    private TextView mNoFeedsMessage;

    private ArrayList<FeedsData> mFeedsList;

    public FeedListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mFeedsList = new ArrayList();
    }

    private void initViews(View root) {
        mNoFeedsMessage = (TextView) root.findViewById(R.id.textview_no_results);
        mRefreshFeedsList = (SwipeRefreshLayout) root.findViewById(R.id.refresh_feeds);
        mRefreshFeedsList.setColorSchemeResources(R.color.colorPrimaryDark, R.color.colorPrimary, R.color.colorAccent, R.color.gray_color);
        mRefreshFeedsList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeeds();
            }
        });

        mFeedRecycler = (RecyclerView) root.findViewById(R.id.listview_feeds);
        mFeedRecycler.setHasFixedSize(false);
        mFeedRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        mFeedRecycler.setAdapter(new FeedsAdapter(getActivity().getApplicationContext(), mFeedsList));

        mFeedRecycler.setVisibility(View.VISIBLE);
        mNoFeedsMessage.setVisibility(View.GONE);

        displayList();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mFeedsList = savedInstanceState.getParcelableArrayList(Extras.FEEDS);
        }

        initViews(getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed_list, container, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(Extras.FEEDS, mFeedsList);
    }

    private void refreshFeeds() {
        if (isConnected()) {
            updateFeeds();
        }
    }

    private void displayList() {
        if (isConnected()) {
            if (mFeedsList == null || mFeedsList.isEmpty()) {
                updateFeeds();
            } else {
                ((FeedsAdapter)mFeedRecycler.getAdapter()).addFeedsList(mFeedsList);
            }
        }
    }

    private RssFeedDownloaderTask feedDownloaderTask;

    private void updateFeeds() {
        if (feedDownloaderTask == null) {
            feedDownloaderTask = new RssFeedDownloaderTask();
            feedDownloaderTask.execute(Extras.FEEDS_URL);
            mRefreshFeedsList.post(new Runnable() {
                @Override
                public void run() {
                    mRefreshFeedsList.setRefreshing(true);
                }
            });
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (feedDownloaderTask != null) {
            feedDownloaderTask.cancel(true);
        }
    }

    private void displayFeedsList() {
        mRefreshFeedsList.setRefreshing(false);
        if (mFeedsList != null && !mFeedsList.isEmpty()) {
            ((FeedsAdapter)mFeedRecycler.getAdapter()).addFeedsList(mFeedsList);
        } else {
            mNoFeedsMessage.setText(R.string.feeds_unavailable);
            mNoFeedsMessage.setVisibility(View.VISIBLE);
        }
        feedDownloaderTask = null;
        System.gc();
    }

    /**
     * RssFeedDownloaderTask to fetch feeds from the network.
     */
    public class RssFeedDownloaderTask extends AsyncTask<String, String, ArrayList<FeedsData>> {

        @Override
        protected ArrayList<FeedsData> doInBackground(String... params) {
            try {
                return NetworkUtils.getOkFeeds(params[0]);
            } catch (IOException io) {
                io.printStackTrace();
                Log.i("RssFeedDownloaderTask", "Issue with IO");
            } catch (JSONException json) {
                Log.i("RssFeedDownloaderTask", "Error parsing JSON data");
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedsData> data) {
            super.onPostExecute(data);
            mFeedsList = data;
            if (!isCancelled()) {
                displayFeedsList();
            }
        }

        @Override
        protected void onPreExecute() {
            mNoFeedsMessage.setVisibility(View.GONE);
        }

    }

    private boolean isConnected() {
        if (AppUtils.isConnectedToNetwork(getActivity().getApplicationContext())) {
            return true;
        } else {
            mRefreshFeedsList.setRefreshing(false);
            //Todo: Add Fake adapter when no data or network is available.
            //mNoFeedsMessage.setText(R.string.network_unavailable);
            //mNoFeedsMessage.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), R.string.network_unavailable, Toast.LENGTH_LONG).show();
        }
        return false;
    }

}
