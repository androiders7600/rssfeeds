package com.sample.image.downloader.model;

/**
 * Created by u471637 on 2/15/16.
 */
public class Extras {

    public static final String FEEDS_URL = "https://public-api.wordpress.com/rest/v1.1/sites/techcrunch.com/posts/";

    public static final String FEED_CATEGORIES = "https://public-api.wordpress.com/rest/v1.1/sites/24588526/categories/";

    public static final String FEEDS = "feed-list";

    public static final String ACTION_UPDATE_FEED_LIST ="update-feeds";

    public static final String ACTION_FEED_DETAILS ="action-feed-details";

    public static final String EXTRA_FEEDS_DATA = "feed-lists";

    public static final String EXTRA_CATEGORIES_DATA = "categories-lists";

    public static final String EXTRA_FEED_DETAILS = "feed-details";



}
