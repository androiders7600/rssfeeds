package com.sample.image.downloader.utils;

import android.text.TextUtils;

import com.sample.image.downloader.model.CategoryData;
import com.sample.image.downloader.model.FeedsData;
import com.sample.image.downloader.model.Posts;
import com.yousuf.image.downloader.utils.RestClientUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by u471637 on 3/8/16.
 */
public class NetworkUtils {

    /**
     * OkHttp Implementation for network calls.
     *
     * @param url
     * @return
     * @throws JSONException
     * @throws IOException
     */
    public static ArrayList<FeedsData> getOkFeeds(String url) throws JSONException, IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            return parseFeeds(response.body().string());
        }
        return null;
    }

    /**
     * Get feeds
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static ArrayList<FeedsData> getFeeds(String url) throws JSONException, IOException {
        String response = RestClientUtils.getContent(url);
        return parseFeeds(response);
    }


    private static ArrayList<FeedsData> parseFeeds(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        ArrayList<FeedsData> feedsList = new ArrayList();
        FeedsData feedsData;
        if (jsonObject != null) {
            JSONArray postsArray = jsonObject.optJSONArray(Posts.ROOT);
            if (postsArray != null) {
                for (int i = 0; i < postsArray.length(); i++) {
                    feedsData = new FeedsData(postsArray.optJSONObject(i));
                    feedsList.add(feedsData);
                }
            }
        }
        return feedsList;
    }

    /**
     * Get feeds
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static ArrayList<CategoryData> getCategories(String url) throws JSONException, IOException {
        String response = RestClientUtils.getContent(url);
        return parseCategories(response);
    }

    private static ArrayList<CategoryData> parseCategories(String response) throws JSONException {
        ArrayList<CategoryData> categoryList = new ArrayList();
        if (!TextUtils.isEmpty(response)) {
            JSONObject jsonObject = new JSONObject(response);
            CategoryData category;
            if (jsonObject != null) {
                JSONArray categoryArray = jsonObject.optJSONArray("categories");
                if (categoryArray != null) {
                    for (int i = 0; i < categoryArray.length(); i++) {
                        category = new CategoryData(categoryArray.optJSONObject(i));
                        categoryList.add(category);
                    }
                }
            }
        }
        return categoryList;
    }

}
