package com.sample.image.downloader.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sample.image.downloader.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yousuf Syed
 */
public class AppUtils {

    /**
     * Get date from timestamp
     *
     * @param ctx       application context.
     * @param timeStamp timestamp in milliseconds.
     * @return formatted age of the post (date or minutes or hours)
     */
    public static String getDate(Context ctx, long timeStamp) {
        final long FULL_DAY = 24 * 60 * 60 * 1000;

        Date date = new Date(timeStamp);
        long timeStampDelta = System.currentTimeMillis() - timeStamp;
        try {
            if (timeStampDelta >= FULL_DAY) {
                return getFormattedDate(date);
            } else {
                return getFormattedTime(ctx, timeStampDelta / (1000 * 60));
            }
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * Get date from date string
     *
     * @param ctx        application context.
     * @param dateString date-time of the post
     * @return formatted age of the post (date or minutes or hours)
     */
    public static String getDate(Context ctx, String dateString) {
        final long FULL_DAY = 24 * 60 * 60 * 1000;


        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
            Date date = format.parse(dateString);

            long timeStamp = date.getTime();
            long timeStampDelta = System.currentTimeMillis() - timeStamp;
            if (timeStampDelta >= FULL_DAY) {
                return getFormattedDate(date);
            } else {
                return getFormattedTime(ctx, timeStampDelta / (1000 * 60));
            }
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * Get formatted age as time.
     *
     * @param ctx            app context
     * @param timeDifference age wrt to current time
     * @return formatted age string
     * @throws ParseException
     */
    public static String getFormattedTime(Context ctx, long timeDifference) throws ParseException {
        long hours = timeDifference / 60;

        if (hours == 0) {
            int min = (int) timeDifference % 60;
            return (min == 1) ? ctx.getString(R.string.one_min_time_string, min) : ctx.getString(R.string.mins_time_string, min);
        } else {
            return (hours == 1) ? ctx.getString(R.string.one_hour_time_string, hours) : ctx.getString(R.string.hours_time_string, hours);
        }
    }

    /**
     * * Get formatted age as date.
     *
     * @param date date when post was added
     * @return formatted date string
     * @throws ParseException
     */
    public static String getFormattedDate(Date date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("MM.dd.yyyy");
        return format.format(date);
    }

    /**
     * Check if device is connected to internet.
     *
     * @param ctx app context
     * @return true if connected, false otherwise.
     */
    public static boolean isConnectedToNetwork(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

}
