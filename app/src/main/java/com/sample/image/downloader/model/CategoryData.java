package com.sample.image.downloader.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ysyed2 on 2/22/16.
 */
public class CategoryData implements Parcelable {
    private int id;         // "ID": 173267257,
    private String name;   //            "name": "Advertising Tech",
    private String slug;  //"slug": "advertising-tech",

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public CategoryData(JSONObject jsonObject) throws JSONException {
        if (jsonObject != null) {
            setId(jsonObject.optInt("ID"));
            setName(jsonObject.optString("name"));
            setSlug(jsonObject.optString("slug"));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public CategoryData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        slug = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(slug);
    }

    public static final Parcelable.Creator<CategoryData> CREATOR = new Creator<CategoryData>() {

        @Override
        public CategoryData createFromParcel(Parcel parcel) {
            return new CategoryData(parcel);
        }

        @Override
        public CategoryData[] newArray(int size) {
            return new CategoryData[size];
        }

    };
}
